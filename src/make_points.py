#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


def get_points_one_dimension(low, high, size):
    """ 
    Returns list of tuples
    generated from array with random points in between intervall from low to high of size=size
    """
    if size % 2 != 0:
        raise ValueError("require even number of particles")
    a = [(np.random.randint(low, high=high) for i in range(1)) for j in range(size)]
    return [tuple(a_i) for a_i in a]
