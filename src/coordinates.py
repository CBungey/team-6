import numpy as np

def read_coords_2D(file):
    """
    Reads coordinates from a CSV file
    :param file: name of the file to be read
    :return: the system coordinates
    """
    coords = np.genfromtxt(file, delimiter=",")

    if len(coords) % 2 != 0:
        raise ValueError("require even number of particles")

    return [(x[0], x[1]) for x in coords]

def make_coords_2D(low, high, size, seed=None):
    """
    Generates a number of points randomly on a grid, determined by upper and lower bounds.
    :param low: lower bound
    :param high: upper bound
    :param size: number of sites
    :param seed: random seed number
    :return: the system coordinates
    """
    if seed is not None:
        np.random.seed(seed)

    if size % 2 != 0:
        raise ValueError("require even number of particles")


    return [(x[0], x[1]) for x in np.random.uniform(low, high, (size, 2))]