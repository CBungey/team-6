from collections import deque
import numpy as np
import time
import datetime

import distance

def round_robin_even(n):
    """
    Makes a list of all possible sets of lists, as indices of the sites.
    :param n: the number of sites.
    :return: the list of combinations.
    """
    d = deque(range(n))

    for i in range(n - 1):
        yield [[d[j], d[-j-1]] for j in range(n//2)]
        d[0], d[-1] = d[-1], d[0]
        d.rotate()

def brute_force(coords):
    """
    Runs every combination of pairs of sites to find the one with the
    minimum accumulative distance.

    :param coords: system coordinates of the sites
    :return: the solution, in the site coordinate.
    """
    print("running brute force approach...")

    n = len(coords)

    pairs_lists = list(round_robin_even(n))

    best_distance = np.inf
    best_pairs = None

    print(f"number of sets of pairs : {len(pairs_lists):^30}")

    start = time.time()
    for pairs in pairs_lists:
        coord_pairs = [(coords[p[0]], coords[p[1]]) for p in pairs]
        d = distance.sum_distance_pairs(coord_pairs)

        if d < best_distance:
            best_distance = d
            best_pairs = coord_pairs
            print(f"current best distance : {d:3.3f}")

    print(f"time for brute force : {datetime.timedelta(seconds=time.time() - start)}")

    return best_pairs
