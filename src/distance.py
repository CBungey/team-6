import numpy as np

def distance(pair):
    """
    Distance between a pair of points
    :param pair: coordinates of the pair of points
    :return: distance
    """
    a = np.array(pair[0])
    b = np.array(pair[1])

    return np.linalg.norm(a - b)

def sum_distance_pairs(pairs):
    """
    Calculates the cumulative distance between all
    pairs
    :param pairs: coordinates of all the pairs of a system
    :return: accumulative distance
    """
    return np.sum([distance(pair) for pair in pairs])
