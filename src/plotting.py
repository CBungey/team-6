import matplotlib.pyplot as plt

def plot_points(coords):
    """
    Plots the system coordinates in a matplotlib figure
    :param coords: system coordinates
    :return: None
    """
    plt.scatter([x[0] for x in coords], [x[1] for x in coords], color='black', marker='x')
    plt.title("Starting Points")
    plt.show()

def plot_solution(solution):
    """
    Plots the system coordinates, along with lines joining pairs, in a matplotlib figure
    :param solution: the solution in system coordinates
    :return: None
    """
    for pair in solution:
        plt.scatter([x[0] for x in pair], [x[1] for x in pair], color='black', marker='x')
        plt.plot([x[0] for x in pair], [x[1] for x in pair], color='red', linestyle='--')

    plt.title("Solution")
    plt.show()
