import argparse
import coordinates
import brute_force
import plotting

def make_parser():
    """
    wrapper for making the argparse object.
    :return: The ArgumentParser object
    """
    parser = argparse.ArgumentParser("Best pairs finder")
    parser.add_argument("--coord_file", type=str, help="csv input file that "
                                                  "contains coordinates")
    parser.add_argument("--n_sites", type=int, help="number of sites")

    parser.add_argument("--plot", action=argparse.BooleanOptionalAction, help="flag for"
                                                                "whether plotting is turned on")

    return parser

def resolve_coords(args):
    """
    Either reads or creates the coordinates for the system, using the input
    flags for logic.
    :param args: input arguments (argparse)

    :return: coordinates of the system
    """
    if args.coord_file is None and args.n_sites is None:
        raise ValueError("either number of sites or coordinate file must be"
                         "given as input")
    elif args.coord_file is not None and args.n_sites is not None:
        raise ValueError("Only ONE of either the number of sites or a coordinate file must be"
                         "given as input")

    elif args.coord_file is not None:
        return coordinates.read_coords_2D(args.coord_file)

    elif args.n_sites is not None:
        return coordinates.make_coords_2D(0, 10, args.n_sites)



if __name__ == "__main__":
    print(f"{'Best Pairs Finder' : ^30}")
    print("-" * 30)

    parser = make_parser()
    args = parser.parse_args()

    coords = resolve_coords(args)

    plot = args.plot is not None

    if plot:
        plotting.plot_points(coords)

    solution = brute_force.brute_force(coords)

    if plot:
        plotting.plot_solution(solution)

    exit(0)