from typing import List, Tuple

def one_dimension(points: List[Tuple] ) -> List[List[Tuple]]:
    sorted_points = sorted(points)
    # Pair up sorted points in 1D
    return [[sorted_points[2*i],sorted_points[2*i+1]] for i in range(len(points) // 2)]