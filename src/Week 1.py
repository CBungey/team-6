"""
Euler Problem #1:

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""
import numpy as np

def oliver_method():
    threes = [x for x in range(1000) if (x%3==0)]
    fives = [x for x in range(1000) if (x%5==0)]

    fifteen = [x for x in range(1000) if (x%15==0)]

    return sum(threes) + sum(fives) - sum(fifteen)

print(f"Oliver Method answer: {oliver_method()}")

def lisa_method(array):
    l = [i for i in array if (i % 3 == 0 or  i % 5 == 0)]
    return sum(l)

array = range(1000)
print('lisa method answer ', lisa_method(array))

def Callum():
    return sum([i for i in range(1000) if not (i%3)*(i%5) ])

print("Callum: {0:d}".format(Callum()))
