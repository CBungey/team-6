import pytest

from typing import List, Tuple

from src.distance import *


@pytest.mark.parametrize("coord, target",
                         [
                             ([(-10), (-1)], 9),
                             ([(-1), (-10)], 9)
                         ])
def test_distance_1d(coord: List[Tuple], target: float) -> bool:
    assert distance(coord) == target


@pytest.mark.parametrize("coord, trgt, correct",
                         [([(0, 0), (0, 3)], 3, True),
                          ([(0, 0), (3, 4)], 5, True),
                          ([(0, 3), (0, 0)], 1, False),
                          ([(1, 1), (4, 5)], 8, False)
                          ]
                         )
def test_distance_2d(coord: List[Tuple], trgt : float, correct : bool) -> bool:
    if correct:
        assert (distance(coord) == trgt)
    else:
        assert (distance(coord) != trgt)

@pytest.mark.parametrize("coordinate, target",
                         [
                             ([(0,0,0),(12,16,21)], 29),
                             ([(0,0,0,0),(0,0,3,4)], 5),
                             ([(1,1,1,1,1),(1,1,1+20,1,1+99)], 101),
                             ([(1.0,2.0,3.0,6.0,4.0,5.0),(0.0,0.0,0.0,11.0,0.0,11.0)],pytest.approx(np.sqrt(91))),
                             ([tuple(1 for i in range(9)),tuple(0 for j in range(9))],3),
                         ])
def test_distance_higher_dimensions(coordinate: List[Tuple] , target : float) -> bool:
    assert distance(coordinate) == target


@pytest.mark.parametrize("coord_list, trgt",
                         [
                             ([[(-10,), (-1,)], [(-2,), (1,)]], 12),
                             ([[(-10,), (-2,)], [(-1,), (1,)]], 10),
                             ([[(-10,), (1,)], [(-1,), (-2,)]], 12),
                             ([[(0,), (1,)], [(10,), (11,)], [(20,), (21,)]], 3)
                         ]
                         )
def test_list_of_pairs_1d(coord_list, trgt):
    assert (sum_distance_pairs(coord_list) == trgt)
