import pytest

from src.coordinates import *


def test_readin_coords():
    test_file = "test/test_csv/test_coords.csv"

    coords = read_coords_2D(test_file)

    assert (coords[0] == (0, 0))
    assert (coords[1] == (0, 1))
    assert (coords[2] == (2, 1))
    assert (coords[3] == (2, 0))



def test_make_coords():
    seed = 15

    coords = make_coords_2D(0, 10, 6, seed=seed)

    assert (round(coords[0][0], 3) == 8.488)
    assert (round(coords[0][1], 3) == 1.789)
    assert (round(coords[1][0], 3) == 0.544)
    assert (round(coords[1][1], 3) == 3.615)
    assert (round(coords[2][0], 3) == 2.754)
    assert (round(coords[2][1], 3) == 5.300)