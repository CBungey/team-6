import pytest

from src.coordinates import *
from src.brute_force import *

def test_brute_force():
    test_file = "test/test_csv/test_coords.csv"

    coords = read_coords_2D(test_file)

    sol = brute_force(coords)

    print(sol)

    assert (sol[0][0] == (0.0, 0.0))
    assert (sol[0][1] == (0.0, 1.0))
    assert (sol[1][0] == (2.0, 1.0))
    assert (sol[1][1] == (2.0, 0.0))

