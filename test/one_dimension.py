import pytest

from src.one_dimension import *

from itertools import permutations

example_points = [(-10,), (-2,), (-1,), (1,)]
input_points = permutations(example_points)

sorted_example_pairs = [[(-10,), (-2,)], [(-1,), (1,)]]

# shuffle the example points for  all orderings.
test_input = [(ordering, sorted_example_pairs) for ordering in input_points]

@pytest.mark.parametrize("point_list, target_pairs",
                         test_input
                         )

def test_one_d(point_list, target_pairs):
    assert one_dimension(point_list) == target_pairs

